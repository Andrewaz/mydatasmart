const  abi =
	 [
		{
			"constant": false,
			"inputs": [
				{
					"name": "_wallet",
					"type": "string"
				},
				{
					"name": "_value",
					"type": "string"
				}
			],
			"name": "setData",
			"outputs": [],
			"payable": false,
			"stateMutability": "nonpayable",
			"type": "function"
		},
		{
			"constant": true,
			"inputs": [
				{
					"name": "_wallet",
					"type": "string"
				}
			],
			"name": "getData",
			"outputs": [
				{
					"name": "",
					"type": "string"
				}
			],
			"payable": false,
			"stateMutability": "view",
			"type": "function"
		},
		{
			"constant": true,
			"inputs": [
				{
					"name": "",
					"type": "uint256"
				}
			],
			"name": "wallets",
			"outputs": [
				{
					"name": "",
					"type": "string"
				}
			],
			"payable": false,
			"stateMutability": "view",
			"type": "function"
		}
	];
module.exports = abi;

